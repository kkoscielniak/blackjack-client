# blackjack-client
Blackjack Game Client

Allows to play the game available [here](https://gitlab.com/kkoscielniak/blackjack-server).

## Installation

```bash
$ yarn
```

## Running the app

```bash
$ yarn start
```

<!-- ## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```
-->
## License

UNLICENSED

