import { FC } from "react";
import "./Modal.style.scss";
import { createPortal } from "react-dom";
import { Balloon, Button, Container, Sprite } from "nes-react";

type Props = {
  isVisible: boolean;
  hideModal: () => void;
};

const Modal: FC<Props> = ({ isVisible, hideModal }) => {
  return isVisible
    ? createPortal(
        <div className="modal-container">
          <div className="modal-overlay" />
          <div className="modal">
            <Container centered>
              <div className="modal-content">
                <Sprite sprite="mario" className="sprite" />
                <Balloon fromLeft className="modal-balloon">
                  It's-a Me, Mario!
                </Balloon>
              </div>
              <Button primary onClick={hideModal}>
                Close
              </Button>
            </Container>
          </div>
        </div>,
        document.body
      )
    : null;
};
export default Modal;
