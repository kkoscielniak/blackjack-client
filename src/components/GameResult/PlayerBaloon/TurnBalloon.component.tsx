import { List } from "nes-react";
import "./MessageBalloon.style.scss";
import { Card } from "../../../models/Card";
import ListItem from "./subcomponents/ListItem.component";
import MessageBalloon from "./MessageBalloon.component";
import {
  getRandomCardsMessage,
  getRandomDrawingMessage,
  getRandomPointsMessage,
} from "./utils/messages";

type Props = {
  cards: string[];
  points: number;
  name: string;
  fromRight?: boolean;
};

const TurnBalloon = ({ cards: propCards, points, name, fromRight }: Props) => {
  const cards = propCards.map((stringifiedCard) => new Card(stringifiedCard));

  return (
    <MessageBalloon name={name} fromRight={fromRight}>
      <>
        <p>
          {getRandomDrawingMessage()}... {getRandomCardsMessage()}:
        </p>
        <List solid>
          {cards.map((card, index) => (
            <ListItem card={card} key={index} />
          ))}
        </List>
        <p>{getRandomPointsMessage(points)}</p>
      </>
    </MessageBalloon>
  );
};

export default TurnBalloon;
