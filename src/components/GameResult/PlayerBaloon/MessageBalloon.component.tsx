import { Balloon } from "nes-react";
import "./MessageBalloon.style.scss";
import c from "classnames";
import { motion } from "framer-motion";

type Props = {
  name: string;
  children?: JSX.Element;
  fromRight?: boolean;
};

const MessageBalloon = ({ name, children, fromRight }: Props) => {
  const animationVariants = {
    visible: { opacity: 1, x: 0 },
    hidden: { opacity: 0, x: fromRight ? 100 : -100 },
  };

  const classNames = c([
    "balloon-container",
    {
      left: !fromRight,
      right: fromRight,
    },
  ]);

  return (
    <motion.div variants={animationVariants} className="motion-container">
      <div className={classNames}>
        {!fromRight ? <p className="playerName">{name}</p> : null}

        <Balloon
          className="balloon"
          fromLeft={!fromRight}
          fromRight={fromRight}
        >
          {children}
        </Balloon>

        {fromRight ? <p className="playerName">{name}</p> : null}
        <br />
      </div>
    </motion.div>
  );
};

export default MessageBalloon;
