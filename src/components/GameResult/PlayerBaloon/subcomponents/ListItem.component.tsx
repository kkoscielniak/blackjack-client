import "../MessageBalloon.component";
import c from "classnames";
import { Card } from "../../../../models/Card";

type Props = {
  card: Card;
};

const ListItem = ({ card }: Props) => {
  const classNames = c([
    "listItem",
    { red: card.isRedCard },
  ]);

  return <li className={classNames}>{card.toString()}</li>;
};

export default ListItem;
