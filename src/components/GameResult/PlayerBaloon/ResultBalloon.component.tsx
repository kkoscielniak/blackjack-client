import "./MessageBalloon.style.scss";
import MessageBalloon from "./MessageBalloon.component";
import { getRandomTieMessage, getRandomWinMessage } from "./utils/messages";

type Props = {
  name: string;
  fromRight?: boolean;
  isTie?: boolean;
};

const ResultBalloon = ({ name, fromRight, isTie }: Props) => {
  return (
    <MessageBalloon name={name} fromRight={fromRight}>
      <p>{isTie ? getRandomTieMessage() : getRandomWinMessage()}</p>
    </MessageBalloon>
  );
};

export default ResultBalloon;
