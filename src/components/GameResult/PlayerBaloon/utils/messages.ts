const getRandomMessage = (messages: string[]): string =>
  messages[Math.floor(Math.random() * messages.length)];

export const getRandomStartingMessage = (): string =>
  getRandomMessage(["Let's play a game!", "Do you want to play a game?"]);

export const getRandomDrawingMessage = (): string =>
  getRandomMessage([
    "Drawing",
    "Okay, drawing",
    "Drawing cards",
    "Yanking",
    "Pulling",
    "Pulling from deck",
  ]);

export const getRandomCardsMessage = (): string =>
  getRandomMessage([
    "I've drawn",
    "I took",
    "I got",
    "I've pulled",
    "I've yanked",
    "And I've got",
  ]);

export const getRandomPointsMessage = (points: number): string =>
  getRandomMessage([
    `Got ${points} points!`,
    `I got ${points}.`,
    `I have ${points} points now!`,
    `I have ${points} points.`,
    `${points}! Noice!`,
  ]);

export const getRandomWinMessage = (): string =>
  getRandomMessage(["I won!", "The victory's mine!", "Yay, I won!", "Blackjack!"]);

export const getRandomTieMessage = (): string =>
  getRandomMessage(["Can't be! It's a tie!", "Tie!", "We got tie!"]);
