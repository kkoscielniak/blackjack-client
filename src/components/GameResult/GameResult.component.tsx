import "./GameResult.style.scss";
import { Button } from "nes-react";
import { ResultType } from "../../models/Game";
import TurnBalloon from "./PlayerBaloon/TurnBalloon.component";
import { motion } from "framer-motion";
import ResultBalloon from "./PlayerBaloon/ResultBalloon.component";

type Props = {
  gameResult: ResultType;
  resetGame: (playerName: string) => void;
};

const GameResult = ({ gameResult, resetGame }: Props) => {
  const protagonistsName = gameResult.players[0].name;
  const handlePlayGame = async () => await resetGame(protagonistsName);

  const animationVariants = {
    visible: {
      opacity: 1,
      transition: {
        when: "beforeChildren",
        staggerChildren: 0.8,
      },
    },
    hidden: {
      opacity: 0,
      transition: {
        when: "afterChildren",
      },
    },
  };

  const { winner, players } = gameResult;
  const {
    firstTurn: protagonistsFirstTurnResult,
    nextTurns: protagonistsNextTurnsResult,
  } = players[0];
  const {
    firstTurn: antagonistsFirstTurnResult,
    nextTurns: antagonistsNextTurnsResult,
    name: antagonistsName,
  } = players[1];

  return (
    <div className="container">
      <motion.div
        variants={animationVariants}
        initial="hidden"
        animate="visible"
      >
        <TurnBalloon
          name={protagonistsName}
          cards={protagonistsFirstTurnResult.cards}
          points={protagonistsFirstTurnResult.points}
        />
        <TurnBalloon
          name={antagonistsName}
          cards={antagonistsFirstTurnResult.cards}
          points={antagonistsFirstTurnResult.points}
          fromRight
        />

        {protagonistsNextTurnsResult.cards.length ? (
          <TurnBalloon
            name={protagonistsName}
            cards={protagonistsNextTurnsResult.cards}
            points={
              protagonistsFirstTurnResult.points +
              protagonistsNextTurnsResult.points
            }
          />
        ) : null}

        {antagonistsNextTurnsResult.cards.length ? (
          <TurnBalloon
            name={antagonistsName}
            cards={antagonistsNextTurnsResult.cards}
            points={
              antagonistsFirstTurnResult.points +
              antagonistsNextTurnsResult.points
            }
            fromRight
          />
        ) : null}

        <ResultBalloon
          name={winner}
          fromRight={winner === antagonistsName}
          isTie={winner.indexOf("Tie") > -1}
        />
        <Button primary onClick={handlePlayGame}>
          Replay!
        </Button>
      </motion.div>
    </div>
  );
};

export default GameResult;
