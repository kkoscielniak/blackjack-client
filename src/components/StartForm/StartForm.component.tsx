import { Button, TextInput } from "nes-react";
import { FC, FormEvent } from "react";

type Props = {
  startGame: (playerName: string) => void;
  playerName: string;
  setPlayerName: (playerName: string) => void;
};

const StartForm: FC<Props> = ({ startGame, playerName, setPlayerName }) => {
  const handlePlayGame = async () => await startGame(playerName);

  const handleTextChange = (event: FormEvent<HTMLInputElement>) =>
    setPlayerName(event?.currentTarget?.value);

  return (
    <>
      <TextInput
        label="Player name"
        labelInline
        placeholder="Alice"
        value={playerName}
        onChange={handleTextChange}
      />
      <Button primary onClick={handlePlayGame}>
        Play!
      </Button>
    </>
  );
};

export default StartForm;
