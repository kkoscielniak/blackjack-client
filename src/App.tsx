import "./App.scss";
import useGame from "./hooks/useGame";
import { Container } from "nes-react";
import StartForm from "./components/StartForm/StartForm.component";
import GameResult from "./components/GameResult/GameResult.component";
import Modal from "./components/Modal/Modal.component";
import useModal from "./hooks/useModal";
import { useKonami } from "react-konami-code";

function App() {
  const { gameResult, startGame, resetGame, playerName, setPlayerName } =
    useGame();

  const { toggleModal, isVisible } = useModal();
  useKonami(toggleModal);

  return (
    <div className="app">
      <Container
        title="♠♣ Blackjack The Game ♥♦"
        rounded
        centered
      >
        <p>It's dangerous to play alone! Try this.</p>
        {!gameResult ? (
          <StartForm
            startGame={startGame}
            playerName={playerName}
            setPlayerName={setPlayerName}
          />
        ) : (
          <GameResult gameResult={gameResult} resetGame={resetGame} />
        )}
      </Container>
      <p className="konami-code">↑ ↑ ↓ ↓ ← → ← → B A</p>
      <Modal isVisible={isVisible} hideModal={toggleModal} />
    </div>
  );
}

export default App;
