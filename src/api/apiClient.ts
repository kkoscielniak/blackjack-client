import {
  create as createApiSauceInstance,
  ApisauceInstance,
  ApiResponse,
} from "apisauce";
import { ResultType } from "../models/Game";

export class ApiClient {
  private readonly httpClient: ApisauceInstance;

  constructor() {
    this.httpClient = createApiSauceInstance({
      baseURL: process.env.REACT_APP_BLACKJACK_SERVER_URL,
    });
  }

  async playGame(playerName?: string): Promise<ResultType | undefined> {
    try {
      const response: ApiResponse<ResultType> = await this.httpClient.get(
        "/game/new",
        {
          name: playerName || "Alice",
          turnBased: true,
        }
      );

      return response.data;
    } catch (error) {
      console.error({ error });
    }
  }
}
