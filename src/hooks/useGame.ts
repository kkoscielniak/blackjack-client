import { useState } from "react";
import { ApiClient } from "../api/apiClient";
import { ResultType } from "../models/Game";

type Game = {
  playerName: string;
  setPlayerName: (playerName: string) => void;
  startGame: (playerName?: string) => Promise<void>;
  resetGame: () => void;
  gameResult: ResultType | null;
};

const useGame = (): Game => {
  const apiClient = new ApiClient();
  const [playerName, setPlayerName] = useState<string>("");
  const [gameResult, setGameResult] = useState<ResultType | null>(null);

  const startGame = async (): Promise<void> => {
    const gameResult = await apiClient.playGame(playerName);
    if (gameResult) {
      setGameResult(gameResult);
    }
  };

  const resetGame = async () => {
    setGameResult(null);
  };

  return { gameResult, startGame, resetGame, playerName, setPlayerName };
};

export default useGame;
