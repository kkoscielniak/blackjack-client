export type PlayerType = {
  name: string;
  points: number;
  cards: string[];
  firstTurn: {
    points: number;
    cards: string[];
  };
  nextTurns: {
    points: number;
    cards: string[];
  };
};

export type ResultType = {
  winner: string;
  players: Array<PlayerType>;
};
