
enum Value {
  TWO = "2",
  THREE = "3",
  FOUR = "4",
  FIVE = "5",
  SIX = "6",
  SEVEN = "7",
  EIGHT = "8",
  NINE = "9",
  TEN = "10",
  JACK = "J",
  QUEEN = "Q",
  KING = "K",
  ACE = "A",
}

export class Card {
  suitSymbol: string;
  value: string;
  readonly isRedCard: boolean;
  readonly points: number;

  constructor(stringifiedCard: string) {
    const suit = stringifiedCard[0];

    this.suitSymbol = this.mapSuitShorthandToSymbol(suit);
    this.value = stringifiedCard.replace(suit, "");

    this.isRedCard = suit === "H" || suit === "D";
    this.points = this.calculatePoints(this.value);
  }

  private calculatePoints(value: string): number {
    switch (value) {
      case Value.ACE:
        return 11;
      case Value.KING:
      case Value.QUEEN:
      case Value.JACK:
        return 10;
      default:
        return Number(value);
    }
  }

  private mapSuitShorthandToSymbol(stringifiedCard: string) {
    return stringifiedCard
      .replace("D", "♦")
      .replace("H", "♥")
      .replace("C", "♣")
      .replace("S", "♠");
  }

  toString(): string {
    return `${this.suitSymbol}${this.value}`;
  }
}
