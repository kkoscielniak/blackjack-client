import { ConversationBuilder } from "../ConversationBuilder";
import { MessageType } from "../Message";

const messageProtagonistDrawsInitially = (points: number) =>
  expect.objectContaining({
    action: MessageType.PROTAGONIST_DRAWS_INITIALLY,
    points,
  });

const messageAntagonistDrawsInitially = (points: number) =>
  expect.objectContaining({
    action: MessageType.ANTAGONIST_DRAWS_INITIALLY,
    points,
  });

const messageProtagonistDraws = (points: number) =>
  expect.objectContaining({
    action: MessageType.PROTAGONIST_DRAWS,
    points,
  });

const messageAntagonistDraws = (points: number) =>
  expect.objectContaining({
    action: MessageType.ANTAGONIST_DRAWS,
    points,
  });

const messageProtagonistWins = () =>
  expect.objectContaining({
    action: MessageType.PROTAGONIST_DRAWS,
  });

const messageAngagonistWins = () =>
  expect.objectContaining({
    action: MessageType.ANTAGONIST_WINS,
  });

const messageTie = () =>
  expect.objectContaining({
    action: MessageType.TIE,
  });

describe("ConversationBuilder", () => {
  it("should build proper conversation for antagonist win", () => {
    const gameResult = {
      winner: "Bob",
      players: [
        { name: "Alice", points: 24, cards: ["D6", "D10", "D8"] },
        { name: "Bob", points: 14, cards: ["H4", "SQ"] },
      ],
    };

    const conversation = new ConversationBuilder(gameResult).build();

    expect(conversation.length).toEqual(4);
    expect(conversation).toEqual(
      expect.arrayContaining([
        messageProtagonistDrawsInitially(16),
        messageAntagonistDrawsInitially(14),
        messageProtagonistDraws(24),
        messageAngagonistWins(),
      ])
    );
  });

  it("should build proper conversation for protagonist win", () => {
    const gameResult = {
      winner: "Alice",
      players: [
        { name: "Alice", points: 19, cards: ["SJ", "D3", "C6"] },
        { name: "Bob", points: 27, cards: ["C8", "S7", "C2", "C10"] },
      ],
    };

    const conversation = new ConversationBuilder(gameResult).build();

    expect(conversation.length).toEqual(5);
    expect(conversation).toEqual(
      expect.arrayContaining([
        messageProtagonistDrawsInitially(13),
        messageAntagonistDrawsInitially(15),
        messageProtagonistDraws(19),
        messageProtagonistWins(),
      ])
    );
  });

  it("should build proper conversation for a tie", () => {
    const gameResult = {
      winner: "Tie!",
      players: [
        { name: "Alice", points: 21, cards: ["D10", "DA"] },
        { name: "Bob", points: 21, cards: ["HA", "CJ"] },
      ],
    };

    const conversation = new ConversationBuilder(gameResult).build();

    expect(conversation.length).toEqual(3);
    expect(conversation).toEqual(
      expect.arrayContaining([
        messageProtagonistDrawsInitially(21),
        messageAntagonistDrawsInitially(21),
        messageTie(),
      ])
    );
  });
});
