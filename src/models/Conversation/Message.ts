import { Card } from "../Card";

export enum PlayerKind {
  PROTAGONIST = "protagonist",
  ANTAGONIST = "antagonist",
}
export const TIE = "Tie";

export enum MessageType {
  PROTAGONIST_DRAWS_INITIALLY = "protagonist_draws_initially",
  ANTAGONIST_DRAWS_INITIALLY = "antagonist_draws_initially",
  PROTAGONIST_DRAWS = "protagonist_draws",
  ANTAGONIST_DRAWS = "antagonist_draws",
  PROTAGONIST_WINS = "protagonist_wins",
  ANTAGONIST_WINS = "antagonist_wins",
  TIE = "tie",
}

export class Message {
  constructor(
    readonly name: string,
    readonly action: MessageType,
    readonly points?: number,
    readonly cards?: Card[]
  ) {}
}
