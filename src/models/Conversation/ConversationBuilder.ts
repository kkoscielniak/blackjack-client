import { Card } from "../Card";
import { MessageType, Message } from "./Message";
import { ResultType } from "../Game";

type Player = {
  name: string;
  cards: Card[];
  points: number;
};

export class ConversationBuilder {
  private protagonist: Player;
  private antagonist: Player;

  private conversation: Message[] = [];

  constructor(gameResult: ResultType) {
    this.protagonist = {
      ...gameResult.players[0],
      cards: gameResult.players[0].cards.map((card) => new Card(card)),
    };

    this.antagonist = {
      ...gameResult.players[1],
      cards: gameResult.players[1].cards.map((card) => new Card(card)),
    };
  }

  private addMessageToConversation(
    playerName: string,
    messageType: MessageType,
    points?: number,
    cards?: Card[]
  ) {
    this.conversation.push(new Message(playerName, messageType, points, cards));
  }

  private calculatePointsInTheFirstRound(playerCards: Card[]): number {
    const [first, second] = playerCards;
    return first.points + second.points;
  }

  private playerWonInTheFirstRound(): boolean {
    if (this.protagonist.points === 21 && this.antagonist.points === 21) {
      this.addMessageToConversation(this.protagonist.name, MessageType.TIE);
      return true;
    }
    if (this.protagonist.points === 21) {
      this.addMessageToConversation(
        this.protagonist.name,
        MessageType.PROTAGONIST_WINS
      );

      return true;
    }
    if (this.antagonist.points === 21) {
      this.addMessageToConversation(
        this.protagonist.name,
        MessageType.ANTAGONIST_WINS
      );

      return true;
    }
    return false;
  }

  private calculateRestOfPoints(cards: Card[]): number {
    return cards.reduce((acc, card) => acc + card.points, 0);
  }

  build(): Message[] {
    const [
      firstProtagonistCard,
      secondProtagonistCard,
      ...restOfProtagonistsCards
    ] = this.protagonist.cards;

    const [
      firstAntagonistCard,
      secondAntagonistCard,
      ...restOfAntagonistsCards
    ] = this.antagonist.cards;

    this.protagonist.points = this.calculatePointsInTheFirstRound(
      this.protagonist.cards
    );

    this.antagonist.points = this.calculatePointsInTheFirstRound(
      this.antagonist.cards
    );

    this.addMessageToConversation(
      this.protagonist.name,
      MessageType.PROTAGONIST_DRAWS_INITIALLY,
      this.protagonist.points,
      [firstProtagonistCard, secondProtagonistCard]
    );

    this.addMessageToConversation(
      this.antagonist.name,
      MessageType.ANTAGONIST_DRAWS_INITIALLY,
      this.antagonist.points,
      [firstAntagonistCard, secondAntagonistCard]
    );

    if (this.playerWonInTheFirstRound()) {
      return this.conversation;
    }

    if (restOfProtagonistsCards.length) {
      this.protagonist.points += this.calculateRestOfPoints(
        restOfProtagonistsCards
      );
      this.addMessageToConversation(
        this.protagonist.name,
        MessageType.PROTAGONIST_DRAWS,
        this.protagonist.points,
        restOfProtagonistsCards.map((card) => card)
      );
    }

    if (this.protagonist.points > 21) {
      this.addMessageToConversation(
        this.antagonist.name,
        MessageType.ANTAGONIST_WINS
      );
      return this.conversation;
    }

    if (restOfAntagonistsCards.length) {
      this.antagonist.points += this.calculateRestOfPoints(
        restOfAntagonistsCards
      );
      this.addMessageToConversation(
        this.antagonist.name,
        MessageType.ANTAGONIST_DRAWS,
        this.antagonist.points,
        restOfAntagonistsCards.map((card) => card)
      );
    }

    if (this.antagonist.points > 21) {
      this.addMessageToConversation(
        this.protagonist.name,
        MessageType.PROTAGONIST_WINS
      );
      return this.conversation;
    }

    this.addMessageToConversation(
      this.antagonist.name,
      MessageType.ANTAGONIST_WINS
    );
    return this.conversation;
  }
}
